Evolver: Meta-optimizing multi-objective metaheuristics
=======================================================

**Evolver is part of the jMetal organization, you can find the code at: [https://github.com/jMetal/Evolver](https://github.com/jMetal/Evolver). This repository is only available to have all my PhD results organized together.**

Evolver is a tool based on the formulation of the automatic configuration and design of multi-objective metaheuristics as a multi-objective optimization problem that can be solved by using the same kind of 
algorithms, i.e., Evolver applies a meta-optimization approach.

The basis of Evolver are:
* A multi-objective metaheuristic algorithm in which, given one or several problems used as training set, a configuration of it is sought that solves the training set in an efficient way. This algorithm is referred as to the *Configurable Algorithm*.
* A design space associated to the configurable algorithm which defines their parameters and components subject to be configured.
* A list of quality indicators used as objectives to minimize when using the configurable algorithm to solve a problem of the training set.
* A *meta-optimizer* algorithm which is used to solve the optimization problem defined by minimizing the quality indicators of an configurable algorithm given a particular training set.

## Further read

José F. Aldana-Martín, Juan J. Durillo, and Antonio J. Nebro. **"Evolver: Meta-optimizing multi-objective metaheuristics"**. In: SoftwareX 24 (2023), p. 101551. ISSN: 2352-7110. DOI: [10.1016/j.softx.2023.101551](https://doi.org/10.1016/j.softx.2023.101551). 